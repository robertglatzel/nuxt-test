// @/plugins/aos.js
// I've went ahead to declare all the config here so it is global
// Read up more here: https://github.com/michalsnik/aos

import AOS from "aos";
import "aos/dist/aos.css";

export default defineNuxtPlugin((nuxtApp) => {
  if (typeof window !== "undefined") {
    nuxtApp.AOS = AOS.init({
      disable: window.innerWidth < 640,
      // offset: 200,
      duration: 1200,
      easing: 'ease-in-out-back',
      // once: false,
      // mirror: true
    }) // eslint-disable-line new-cap
  }
})